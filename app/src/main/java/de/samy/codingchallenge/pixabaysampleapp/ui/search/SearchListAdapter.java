package de.samy.codingchallenge.pixabaysampleapp.ui.search;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.Objects;

import androidx.databinding.DataBindingComponent;
import androidx.databinding.DataBindingUtil;
import de.samy.codingchallenge.pixabaysampleapp.R;
import de.samy.codingchallenge.pixabaysampleapp.databinding.SearchItemBinding;
import de.samy.codingchallenge.pixabaysampleapp.ui.common.DataBoundListAdapter;
import de.samy.codingchallenge.pixabaysampleapp.vo.PictureHit;

/**
 * A RecyclerView adapter.
 */
public class SearchListAdapter extends DataBoundListAdapter<PictureHit, SearchItemBinding> {
    private final androidx.databinding.DataBindingComponent dataBindingComponent;
    private final PictureHitClickCallback pictureHitClickCallback;

    SearchListAdapter(DataBindingComponent dataBindingComponent,
                      PictureHitClickCallback pictureHitClickCallback) {
        this.dataBindingComponent = dataBindingComponent;
        this.pictureHitClickCallback = pictureHitClickCallback;
    }

    @Override
    protected SearchItemBinding createBinding(ViewGroup parent) {
        SearchItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.search_item,
                        parent, false, dataBindingComponent);
        binding.getRoot().setOnClickListener(v -> {
            PictureHit pictureHit = binding.getPictureHit();
            if (pictureHit != null && pictureHitClickCallback != null) {
                pictureHitClickCallback.onClick(pictureHit);
            }
        });
        return binding;
    }

    @Override
    protected void bind(SearchItemBinding binding, PictureHit item) {
        binding.setPictureHit(item);
    }

    @Override
    protected boolean areItemsTheSame(PictureHit oldItem, PictureHit newItem) {
        return Objects.equals(oldItem.getId(), newItem.getId());
    }

    @Override
    protected boolean areContentsTheSame(PictureHit oldItem, PictureHit newItem) {
        return Objects.equals(oldItem.getLargeImageURL(), newItem.getLargeImageURL());
    }

    public interface PictureHitClickCallback {
        void onClick(PictureHit pictureHit);
    }
}
