package de.samy.codingchallenge.pixabaysampleapp.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import de.samy.codingchallenge.pixabaysampleapp.MainActivity;

@Module
public abstract class MainActivityModule {
    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract MainActivity contributeMainActivity();
}
