package de.samy.codingchallenge.pixabaysampleapp.repository;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import de.samy.codingchallenge.pixabaysampleapp.AppExecutors;
import de.samy.codingchallenge.pixabaysampleapp.api.ApiResponse;
import de.samy.codingchallenge.pixabaysampleapp.vo.Resource;

public abstract class NetworkBoundResource<RequestType> {

    private final MediatorLiveData<Resource<RequestType>> result = new MediatorLiveData<>();

    @MainThread
    NetworkBoundResource(AppExecutors appExecutors) {
        result.setValue(Resource.loading(null));

        LiveData<ApiResponse<RequestType>> apiResponse = createCall();
        result.addSource(apiResponse, response -> {
            result.removeSource(apiResponse);
            if (response.isSuccessful()) {
                appExecutors.mainThread().execute(() -> {
                    result.setValue(Resource.success(apiResponse.getValue().getData()));
                    asLiveData();
                });
            } else {
                result.setValue(Resource.error(apiResponse.getValue().getError().getMessage()));
                asLiveData();
            }
        });
    }

    public LiveData<Resource<RequestType>> asLiveData() {
        return result;
    }

    @NonNull
    @MainThread
    protected abstract LiveData<ApiResponse<RequestType>> createCall();
}
