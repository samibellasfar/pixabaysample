package de.samy.codingchallenge.pixabaysampleapp.api;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import de.samy.codingchallenge.pixabaysampleapp.vo.PictureHit;

public class PixaBaySearchResponse {

    @SerializedName("totalHits")
    private int totalHits;

    @SerializedName("total")
    private int total;

    @SerializedName("hits")
    private List<PictureHit> pictureHitsList;

    public PixaBaySearchResponse() {
    }

    public PixaBaySearchResponse(int totalHits, int total, List<PictureHit> pictureHitsList) {
        this.totalHits = totalHits;
        this.total = total;
        this.pictureHitsList = pictureHitsList;
    }

    public int getTotalHits() {
        return totalHits;
    }

    public void setTotalHits(int totalHits) {
        this.totalHits = totalHits;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<PictureHit> getPictureHitsList() {
        return pictureHitsList;
    }

    public void setPictureHitsList(List<PictureHit> pictureHitsList) {
        this.pictureHitsList = pictureHitsList;
    }
}
