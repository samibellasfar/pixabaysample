package de.samy.codingchallenge.pixabaysampleapp.repository;

import javax.inject.Inject;
import javax.inject.Singleton;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import de.samy.codingchallenge.pixabaysampleapp.AppExecutors;
import de.samy.codingchallenge.pixabaysampleapp.BuildConfig;
import de.samy.codingchallenge.pixabaysampleapp.api.ApiResponse;
import de.samy.codingchallenge.pixabaysampleapp.api.PixaBaySearchResponse;
import de.samy.codingchallenge.pixabaysampleapp.api.PixaBayService;
import de.samy.codingchallenge.pixabaysampleapp.vo.Resource;

@Singleton
public class SearchResultRepository {

    private final PixaBayService pixaBayService;

    private final AppExecutors appExecutors;

    @Inject
    public SearchResultRepository(AppExecutors appExecutors,
                                  PixaBayService pixaBayService) {
        this.pixaBayService = pixaBayService;
        this.appExecutors = appExecutors;
    }

    public LiveData<Resource<PixaBaySearchResponse>> search(String query) {

        return new NetworkBoundResource<PixaBaySearchResponse>(appExecutors) {

            @NonNull
            @Override
            protected LiveData<ApiResponse<PixaBaySearchResponse>> createCall() {
                return pixaBayService.searchImages(BuildConfig.PIXABAY_API_KEY, query);
            }
        }.asLiveData();
    }
}
