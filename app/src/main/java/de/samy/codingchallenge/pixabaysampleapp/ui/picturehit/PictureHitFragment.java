package de.samy.codingchallenge.pixabaysampleapp.ui.picturehit;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import de.samy.codingchallenge.pixabaysampleapp.R;
import de.samy.codingchallenge.pixabaysampleapp.binding.FragmentDataBindingComponent;
import de.samy.codingchallenge.pixabaysampleapp.databinding.FragmentPicturehitBinding;
import de.samy.codingchallenge.pixabaysampleapp.di.Injectable;
import de.samy.codingchallenge.pixabaysampleapp.utils.AutoClearedValue;
import de.samy.codingchallenge.pixabaysampleapp.vo.PictureHit;

/**
 * A simple {@link Fragment} subclass.
 */
public class PictureHitFragment extends Fragment implements Injectable {
    private static final String PICTURE_HIT_USERNAME_ARG = "PICTURE_HIT_USERNAME_ARG";
    private static final String PICTURE_HIT_BIG_IMAGE_URL_ARG = "PICTURE_HIT_BIG_IMAGE_URL_ARG";
    private static final String PICTURE_HIT_TAGS_ARG = "PICTURE_HIT_TAGS_ARG";
    private static final String PICTURE_HIT_LIKES_ARG = "PICTURE_HIT_LIKES_ARG";
    private static final String PICTURE_HIT_FAVOURITES_ARG = "PICTURE_HIT_FAVOURITES_ARG";
    private static final String PICTURE_HIT_COMMENTS_ARG = "PICTURE_HIT_COMMENTS_ARG";

    private DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    AutoClearedValue<FragmentPicturehitBinding> binding;

    public PictureHitFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentPicturehitBinding dataBinding = DataBindingUtil
                .inflate(inflater, R.layout.fragment_picturehit, container, false,
                        dataBindingComponent);
        binding = new AutoClearedValue<>(this, dataBinding);
        return dataBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getArguments() != null) {
            PictureHit pictureHit = new PictureHit();
            pictureHit.setLargeImageURL(getArguments().getString(PICTURE_HIT_BIG_IMAGE_URL_ARG));
            pictureHit.setUser(getArguments().getString(PICTURE_HIT_USERNAME_ARG));
            pictureHit.setTags(getArguments().getString(PICTURE_HIT_TAGS_ARG));
            pictureHit.setLikes(getArguments().getInt(PICTURE_HIT_LIKES_ARG));
            pictureHit.setFavorites(getArguments().getInt(PICTURE_HIT_FAVOURITES_ARG));
            pictureHit.setComments(getArguments().getInt(PICTURE_HIT_COMMENTS_ARG));
            binding.get().setPictureHit(pictureHit);
        } else {
            throw new IllegalStateException("Should have added some params.");
        }
    }

    public static PictureHitFragment newInstance(PictureHit pictureHit) {
        PictureHitFragment pictureHitFragment = new PictureHitFragment();
        Bundle args = new Bundle();
        args.putString(PICTURE_HIT_BIG_IMAGE_URL_ARG, pictureHit.getLargeImageURL());
        args.putString(PICTURE_HIT_USERNAME_ARG, pictureHit.getUser());
        args.putString(PICTURE_HIT_TAGS_ARG, pictureHit.getTags());
        args.putInt(PICTURE_HIT_LIKES_ARG, pictureHit.getLikes());
        args.putInt(PICTURE_HIT_FAVOURITES_ARG, pictureHit.getFavorites());
        args.putInt(PICTURE_HIT_COMMENTS_ARG, pictureHit.getComments());
        pictureHitFragment.setArguments(args);
        return pictureHitFragment;
    }
}
