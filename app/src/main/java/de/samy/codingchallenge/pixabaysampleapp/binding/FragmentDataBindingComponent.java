package de.samy.codingchallenge.pixabaysampleapp.binding;

import androidx.databinding.DataBindingComponent;
import androidx.fragment.app.Fragment;

public class FragmentDataBindingComponent implements DataBindingComponent {
    private final FragmentBindingAdapters fragmentBindingAdapters;

    public FragmentDataBindingComponent(Fragment fragment) {
        fragmentBindingAdapters = new FragmentBindingAdapters(fragment);
    }

    @Override
    public FragmentBindingAdapters getFragmentBindingAdapters() {
        return fragmentBindingAdapters;
    }
}
