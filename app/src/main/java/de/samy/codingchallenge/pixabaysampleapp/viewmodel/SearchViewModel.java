package de.samy.codingchallenge.pixabaysampleapp.viewmodel;

import java.util.Locale;
import java.util.Objects;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import de.samy.codingchallenge.pixabaysampleapp.api.PixaBaySearchResponse;
import de.samy.codingchallenge.pixabaysampleapp.repository.SearchResultRepository;
import de.samy.codingchallenge.pixabaysampleapp.utils.AbsentLiveData;
import de.samy.codingchallenge.pixabaysampleapp.vo.Resource;

public class SearchViewModel extends ViewModel {

    private final MutableLiveData<String> query = new MutableLiveData<>();

    private final LiveData<Resource<PixaBaySearchResponse>> result;

    @Inject
    SearchViewModel(SearchResultRepository searchResultRepository) {
        result = Transformations.switchMap(query, search -> {
            if (search == null || search.trim().length() == 0) {
                return AbsentLiveData.create();
            } else {
                return searchResultRepository.search(search);
            }
        });
    }

    public LiveData<Resource<PixaBaySearchResponse>> getResult() {
        return result;
    }

    public void setQuery(@NonNull String originalInput) {
        String input = originalInput.toLowerCase(Locale.getDefault()).trim();
        if (Objects.equals(input, query.getValue())) {
            return;
        }
        query.setValue(input);
    }
}
