package de.samy.codingchallenge.pixabaysampleapp.ui.search;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import de.samy.codingchallenge.pixabaysampleapp.R;
import de.samy.codingchallenge.pixabaysampleapp.binding.FragmentDataBindingComponent;
import de.samy.codingchallenge.pixabaysampleapp.databinding.FragmentSearchBinding;
import de.samy.codingchallenge.pixabaysampleapp.di.Injectable;
import de.samy.codingchallenge.pixabaysampleapp.ui.common.NavigationController;
import de.samy.codingchallenge.pixabaysampleapp.utils.AutoClearedValue;
import de.samy.codingchallenge.pixabaysampleapp.viewmodel.SearchViewModel;
import de.samy.codingchallenge.pixabaysampleapp.vo.PictureHit;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment implements Injectable {
    private static final String SEARCH_DEFAULT_INPUT = "SEARCH_DEFAULT_INPUT_ARG";

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Inject
    NavigationController navigationController;

    private DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);

    private AutoClearedValue<FragmentSearchBinding> binding;

    private AutoClearedValue<SearchListAdapter> adapter;

    private SearchViewModel searchViewModel;

    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FragmentSearchBinding dataBinding = DataBindingUtil
                .inflate(inflater, R.layout.fragment_search, container, false,
                        dataBindingComponent);
        binding = new AutoClearedValue<>(this, dataBinding);
        return dataBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        searchViewModel = ViewModelProviders.of(this, viewModelFactory).get(SearchViewModel.class);
        SearchListAdapter rvAdapter = new SearchListAdapter(dataBindingComponent,
                this::showYesNoDialog);
        adapter = new AutoClearedValue<>(this, rvAdapter);

        initRecyclerView();
        initSearchInputListener();
        binding.get().repoList.setAdapter(rvAdapter);

        if (getArguments() != null && getArguments().containsKey(SEARCH_DEFAULT_INPUT)) {
            String initialSearch = getArguments().getString(SEARCH_DEFAULT_INPUT);
            EditText input = binding.get().input;
            input.setText(initialSearch);
            doSearch(input);
        }
    }

    private void initRecyclerView() {
        searchViewModel.getResult().observe(this, result -> {
            binding.get().setSearchResource(result);
            adapter.get().replace((result != null &&
                    result.data != null) ? result.data.getPictureHitsList() : null);
            binding.get().executePendingBindings();
        });
    }

    private void initSearchInputListener() {
        binding.get().input.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                doSearch(v);
                return true;
            }
            return false;
        });
        binding.get().input.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.ACTION_DOWN)
                    && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                doSearch(v);
                return true;
            }
            return false;
        });
    }

    private void doSearch(View v) {
        String query = binding.get().input.getText().toString();
        dismissKeyboard(v.getWindowToken());
        binding.get().setQuery(query);
        searchViewModel.setQuery(query);
    }

    private void dismissKeyboard(IBinder windowToken) {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(windowToken, 0);
            }
        }
    }

    private void showYesNoDialog(PictureHit pictureHit) {
        if (isAdded()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

            builder.setTitle(R.string.dialog_title);
            builder.setMessage(R.string.dialog_description);

            builder.setPositiveButton(R.string.dialog_yes, (dialog, which) -> {
                navigationController.navigateToPictureHitDetail(pictureHit);
                dialog.dismiss();
            });

            builder.setNegativeButton(R.string.dialog_no, (dialog, which) -> dialog.dismiss());

            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    public static SearchFragment newInstance(String defaultSearch) {
        SearchFragment searchFragment = new SearchFragment();
        Bundle args = new Bundle();
        args.putString(SEARCH_DEFAULT_INPUT, defaultSearch);
        searchFragment.setArguments(args);
        return searchFragment;
    }
}
