package de.samy.codingchallenge.pixabaysampleapp.api;

import androidx.lifecycle.LiveData;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PixaBayService {

    @GET("api/")
    LiveData<ApiResponse<PixaBaySearchResponse>> searchImages(@Query("key") String key,
                                                              @Query("q") String query);
}
