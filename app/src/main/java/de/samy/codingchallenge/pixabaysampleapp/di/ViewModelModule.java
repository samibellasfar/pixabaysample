package de.samy.codingchallenge.pixabaysampleapp.di;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import de.samy.codingchallenge.pixabaysampleapp.viewmodel.SearchViewModel;
import de.samy.codingchallenge.pixabaysampleapp.viewmodel.SearchViewModelFactory;

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(SearchViewModel.class)
    abstract ViewModel bindSearchViewModel(SearchViewModel searchViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(SearchViewModelFactory factory);
}
