package de.samy.codingchallenge.pixabaysampleapp.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import de.samy.codingchallenge.pixabaysampleapp.ui.picturehit.PictureHitFragment;
import de.samy.codingchallenge.pixabaysampleapp.ui.search.SearchFragment;

@Module
public abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract SearchFragment contributeSearchFragment();

    @ContributesAndroidInjector
    abstract PictureHitFragment contributePictureHitFragment();
}
