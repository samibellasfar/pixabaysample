package de.samy.codingchallenge.pixabaysampleapp.ui.common;

import javax.inject.Inject;

import androidx.fragment.app.FragmentManager;
import de.samy.codingchallenge.pixabaysampleapp.MainActivity;
import de.samy.codingchallenge.pixabaysampleapp.R;
import de.samy.codingchallenge.pixabaysampleapp.ui.picturehit.PictureHitFragment;
import de.samy.codingchallenge.pixabaysampleapp.ui.search.SearchFragment;
import de.samy.codingchallenge.pixabaysampleapp.vo.PictureHit;

/**
 * A utility class that handles navigation in {@link MainActivity}.
 */
public class NavigationController {
    private static final String INITIAL_SEARCH = "fruits";

    private final int containerId;
    private final FragmentManager fragmentManager;

    @Inject
    public NavigationController(MainActivity mainActivity) {
        this.containerId = R.id.container;
        this.fragmentManager = mainActivity.getSupportFragmentManager();
    }

    public void navigateToSearch() {
        SearchFragment searchFragment = SearchFragment.newInstance(INITIAL_SEARCH);
        fragmentManager.beginTransaction()
                .replace(containerId, searchFragment)
                .commitAllowingStateLoss();
    }

    public void navigateToPictureHitDetail(PictureHit pictureHit) {
        PictureHitFragment pictureHitFragment = PictureHitFragment.newInstance(pictureHit);
        fragmentManager.beginTransaction()
                .replace(containerId, pictureHitFragment)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }
}
