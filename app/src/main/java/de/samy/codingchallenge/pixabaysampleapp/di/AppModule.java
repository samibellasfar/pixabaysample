package de.samy.codingchallenge.pixabaysampleapp.di;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import de.samy.codingchallenge.pixabaysampleapp.api.PixaBayService;
import de.samy.codingchallenge.pixabaysampleapp.utils.LiveDataCallAdapterFactory;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = ViewModelModule.class)
class AppModule {

    private final static int cacheSize = 10 * 1024 * 1024; // 10 MB of cache

    @Singleton
    @Provides
    PixaBayService providePixaBayService(Application app) {
        Cache cache = new Cache(app.getCacheDir(), cacheSize);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .cache(cache)
                .build();
        return new Retrofit.Builder()
                .baseUrl("https://pixabay.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                .client(okHttpClient)
                .build()
                .create(PixaBayService.class);
    }
}
